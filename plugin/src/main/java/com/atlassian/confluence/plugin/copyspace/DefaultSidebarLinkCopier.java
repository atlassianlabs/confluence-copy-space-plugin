package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.plugins.ia.SidebarLink;
import com.atlassian.confluence.plugins.ia.SidebarLinkManager;
import com.atlassian.confluence.plugins.ia.SidebarLinks;
import com.atlassian.confluence.spaces.Space;

import java.util.Map;
import java.util.stream.Collectors;

public class DefaultSidebarLinkCopier implements SidebarLinkCopier {

    private final SidebarLinkManager sidebarLinkManager;

    public DefaultSidebarLinkCopier(SidebarLinkManager sidebarLinkManager) {
        this.sidebarLinkManager = sidebarLinkManager;
    }

    @Override
    public void copySidebarLinks(Space originalSpace, Space newSpace, SpaceCopyContext spaceCopyContext) {
        final Map<Long, Long> pageIdPairs = spaceCopyContext.getCopiedPages().stream().collect(
                Collectors.toMap(pair -> pair.original.getId(), pair -> pair.copy.getId()));
        final Map<Long, Long> attachmentIdPairs = spaceCopyContext.getCopiedAttachments().stream().collect(
                Collectors.toMap(pair -> pair.original.getId(), pair -> pair.copy.getId()));
        final SidebarLinks originalLinks = sidebarLinkManager.findBySpace(originalSpace.getKey());
        originalLinks.getAllLinks().forEach(link -> copySidebarLink(link, newSpace.getKey(), pageIdPairs, attachmentIdPairs));
    }

    private void copySidebarLink(SidebarLink link, String newSpaceKey, Map<Long, Long> pageIdPairs,
                                 Map<Long, Long> attachmentIdPairs) {
        switch (link.getType()) {
            case PINNED_PAGE:
                copyLinkReplacingReference(link, newSpaceKey, pageIdPairs);
                break;
            case PINNED_ATTACHMENT:
                copyLinkReplacingReference(link, newSpaceKey, attachmentIdPairs);
                break;
            default:
                createLink(link, newSpaceKey, link.getDestPageId());
        }
    }

    private void copyLinkReplacingReference(SidebarLink link, String newSpaceKey, Map<Long, Long> contentIdPairs) {
        final Long contentCopyId = contentIdPairs.get(link.getDestPageId());
        final Long destContentId = contentCopyId != null ? contentCopyId : link.getDestPageId();
        createLink(link, newSpaceKey, destContentId);
    }

    private void createLink(SidebarLink link, String newSpaceKey, Long destContentId) {
        sidebarLinkManager.createLink(
                newSpaceKey,
                link.getCategory(),
                link.getType(),
                link.getWebItemKey(),
                link.getPosition(),
                link.getCustomTitle(),
                link.getHardcodedUrl(),
                link.getCustomIconClass(),
                destContentId
        );
    }
}
