package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;

import java.util.ArrayList;
import java.util.List;

public class SpaceCopyContext {

    private final List<ContentPair<Page>> copiedPages = new ArrayList<>();
    private final List<ContentPair<Attachment>> copiedAttachments = new ArrayList<>();

    public List<ContentPair<Page>> getCopiedPages() {
        return new ArrayList<>(copiedPages);
    }

    public List<ContentPair<Attachment>> getCopiedAttachments() {
        return new ArrayList<>(copiedAttachments);
    }

    public void addPagePair(ContentPair<Page> pagePair) {
        copiedPages.add(pagePair);
    }

    public void addAttachmentPair(ContentPair<Attachment> attachmentPair) {
        copiedAttachments.add(attachmentPair);
    }

    static class ContentPair<T> {
        public final T original;
        public final T copy;

        public ContentPair(T oldContent, T newContent) {
            this.original = oldContent;
            this.copy = newContent;
        }
    }
}
