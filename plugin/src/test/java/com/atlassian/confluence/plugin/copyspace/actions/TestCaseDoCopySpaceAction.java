package com.atlassian.confluence.plugin.copyspace.actions;

import com.atlassian.confluence.plugin.copyspace.CopySpaceManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.opensymphony.xwork.Action;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestCaseDoCopySpaceAction {

    private static final String NEW_SPACE_NAME_EMPTY_KEY = "space.name.empty";
    private static final String NEW_SPACE_KEY_INVALID_KEY = "space.key.invalid";
    private static final String NEW_SPACE_KEY_EXISTS_KEY = "space.key.exists";
    private static final String NEW_SPACE_KEY_EMPTY_KEY = "space.key.empty";
    private static final String NEW_SPACE_NAME_EMPTY_VALUE = "You must enter a space name.";
    private static final String NEW_SPACE_KEY_INVALID_VALUE = "Space keys may only consist of ASCII letters or numbers (A-Z, a-z, 0-9)";
    private static final String NEW_SPACE_KEY_EXISTS_VALUE = "A space with this space key already exists.";
    private static final String NEW_SPACE_KEY_EMPTY_VALUE = "You must enter a space key.";
    private static final String NEW_SPACE_NAME = "New Space";
    private static final String NEW_SPACE_KEY = "NEW";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private CopySpaceManager copySpaceManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private SpaceManager spaceManager;
    @Mock
    private I18NBean i18NBean;
    @Mock
    private Space originalSpace;
    @Mock
    private ConfluenceUser user;

    private DoCopySpaceAction doCopySpaceAction;

    @Before
    public void setUp() {
        AuthenticatedUserThreadLocal.set(user);
        setUpCopySpaceAction();
        setUpI18n();
    }

    private void setUpI18n() {
        final Map<String, String> i18n = new HashMap<>();
        i18n.put(NEW_SPACE_NAME_EMPTY_KEY, NEW_SPACE_NAME_EMPTY_VALUE);
        i18n.put(NEW_SPACE_KEY_INVALID_KEY, NEW_SPACE_KEY_INVALID_VALUE);
        i18n.put(NEW_SPACE_KEY_EXISTS_KEY, NEW_SPACE_KEY_EXISTS_VALUE);
        i18n.put(NEW_SPACE_KEY_EMPTY_KEY, NEW_SPACE_KEY_EMPTY_VALUE);
        i18n.forEach((k, v) -> when(i18NBean.getText(eq(k), anyList())).thenReturn(v));
    }

    private void setUpCopySpaceAction() {
        doCopySpaceAction = new DoCopySpaceAction();
        doCopySpaceAction.setCopySpaceManager(copySpaceManager);
        doCopySpaceAction.setSpace(originalSpace);
        doCopySpaceAction.setSpaceManager(spaceManager);
        doCopySpaceAction.setNewKey(NEW_SPACE_KEY);
        doCopySpaceAction.setNewName(NEW_SPACE_NAME);
        doCopySpaceAction.setI18NBean(i18NBean);
    }

    @Test
    public void testSpaceCopiedUnderNewKey() throws Exception {
        Space newSpace = new Space();
        newSpace.setKey(NEW_SPACE_KEY);
        newSpace.setName(NEW_SPACE_NAME);

        doCopySpaceAction.setSpace(originalSpace);
        doCopySpaceAction.setNewKey(NEW_SPACE_KEY);
        doCopySpaceAction.setNewName(NEW_SPACE_NAME);

        assertEquals(Action.SUCCESS, doCopySpaceAction.execute());
        assertEquals(NEW_SPACE_KEY, doCopySpaceAction.getKey());

        verify(copySpaceManager, atLeastOnce()).copySpace(eq(originalSpace), eq(NEW_SPACE_KEY), eq(NEW_SPACE_NAME), eq(user), anyObject());
    }

    @Test
    public void testAdminPrivilegeRequiredToCopySpace() {
        doCopySpaceAction.setPermissionManager(permissionManager);

        when(permissionManager.hasCreatePermission(user, PermissionManager.TARGET_APPLICATION, Space.class)).thenReturn(true);
        when(permissionManager.hasPermission(user, Permission.ADMINISTER, originalSpace)).thenReturn(false);

        assertFalse(doCopySpaceAction.isPermitted());
    }

    @Test
    public void testSpaceNameIsNullValidation() throws Exception {
        doCopySpaceAction.setNewName(null);
        assertEquals(Action.ERROR, doCopySpaceAction.execute());
        assertEquals(NEW_SPACE_NAME_EMPTY_VALUE, doCopySpaceAction.getErrorMessage());
    }

    @Test
    public void testSpaceNameIsEmptyValidation() throws Exception {
        doCopySpaceAction.setNewName("");
        assertEquals(Action.ERROR, doCopySpaceAction.execute());
        assertEquals(NEW_SPACE_NAME_EMPTY_VALUE, doCopySpaceAction.getErrorMessage());
    }

    @Test
    public void testSpaceKeyIsNotGlobalValid() throws Exception {
        doCopySpaceAction.setNewKey("#");
        assertEquals(Action.ERROR, doCopySpaceAction.execute());
        assertEquals(NEW_SPACE_KEY_INVALID_VALUE, doCopySpaceAction.getErrorMessage());
    }

    @Test
    public void testSpaceKeyIsNullValidation() throws Exception {
        doCopySpaceAction.setNewKey(null);
        assertEquals(Action.ERROR, doCopySpaceAction.execute());
        assertEquals(NEW_SPACE_KEY_EMPTY_VALUE, doCopySpaceAction.getErrorMessage());
    }

    @Test
    public void testSpaceKeyIsEmptyValidation() throws Exception {
        doCopySpaceAction.setNewKey("");
        assertEquals(Action.ERROR, doCopySpaceAction.execute());
        assertEquals(NEW_SPACE_KEY_EMPTY_VALUE, doCopySpaceAction.getErrorMessage());
    }

    @Test
    public void testSpaceExistsWithGivenKeyValidation() throws Exception {
        final Space existingSpace = mock(Space.class);
        when(spaceManager.getSpace(eq(NEW_SPACE_KEY))).thenReturn(existingSpace);
        doCopySpaceAction.setNewKey(NEW_SPACE_KEY);

        assertEquals(Action.ERROR, doCopySpaceAction.execute());
        assertEquals(NEW_SPACE_KEY_EXISTS_VALUE, doCopySpaceAction.getErrorMessage());
    }
}
